﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    public class ClienteModelo
    {
        private static List<Cliente> ListClientes = new List<Cliente>();
        private implementacion.DaoImplementsCliente daocliente;

        public ClienteModelo()
        {
            daocliente = new implementacion.DaoImplementsCliente();
        }
        public List<Cliente> GetListCliente()
        {
            return daocliente.FindAll();
        }

        public void Populate()
        {
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(Properties.Resources.Cliente_data));
            foreach (Cliente c in ListClientes)
            {
                daocliente.Save(c);
            }
        }

        public void Save(DataRow cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(cliente["Id"].ToString());
            c.Cedula = cliente["Cedula"].ToString();
            c.Nombres = cliente["Nombres"].ToString();
            c.Apellidos = cliente["Apellidos"].ToString();
            c.Telefono = cliente["Telefono"].ToString();
            c.Correo = cliente["Correo"].ToString();
            c.Direccion = cliente["Direccion"].ToString();

            daocliente.Save(c);

        }

    }
}

