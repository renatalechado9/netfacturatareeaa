﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> productos = new List<Producto>();


        public static List<Producto> GetProductos()
        {
            return productos;
        }

        public static void Populate()
        {

            productos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            
        }
    }
}
