﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.utilities;
using NETProjectTutorial.dao;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.implementacion
{
    public class DaoImplementsCliente : DaoCliente
    {
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;
        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 980;

        private List<HeaderIndex> headerIndices;
        private Header header;


        public DaoImplementsCliente() { }

        private void Open() {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "IdCliente"
                    },
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "CedulaCliente"
                    },
                     new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "ApellidosCliente"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hCliente",
                    HeaderIndices = headerIndices
                };


                fsHeader = new FileStream("hCliente.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dCliente.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//n
                    bwHeader.Write(0);//k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }

        }

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }


        public Cliente FindByCedula(string cedula)
        {
            Cliente cliente = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dCedula = brData.ReadString();

                    if (!cedula.Equals(dCedula, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    cliente = new Cliente()
                    {
                        Id = dId,
                        Cedula = dCedula,
                        Nombres = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Direccion = brData.ReadString(),
                        Telefono = brData.ReadString(),
                        Correo = brData.ReadString()

                    };
                    break;
                }


                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return  cliente;

        }

        public Cliente FindById(int id)
        {
            Cliente cliente = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if (index < 0)
                {
                    return cliente;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                cliente = new Cliente()
                {
                    Id = brData.ReadInt32(),
                    Cedula = brData.ReadString(),
                    Nombres = brData.ReadString(),
                    Apellidos = brData.ReadString(),
                    Direccion = brData.ReadString(),
                    Telefono = brData.ReadString(),
                    Correo = brData.ReadString()
                };

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return cliente;

        }

        public List<Cliente> FindByLastname(string lastname)
        {
            List<Cliente> clientes = new List<Cliente>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Cliente cliente = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dLastname = brData.ReadString();

                    if (!lastname.Equals(dLastname, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    cliente = new Cliente()
                    {
                        Id = dId,
                        Cedula = brData.ReadString(),
                        Nombres = brData.ReadString(),
                        Apellidos = dLastname,
                        Direccion = brData.ReadString(),
                        Telefono = brData.ReadString(),
                        Correo = brData.ReadString()

                    };
                    break;
                }

                clientes.Add(cliente);
                
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return clientes;
        }

        public void Save(Cliente t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.Cedula);
                bwData.Write(t.Nombres);
                bwData.Write(t.Apellidos);
                bwData.Write(t.Direccion);
                bwData.Write(t.Telefono);
                bwData.Write(t.Correo);

                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }

        public bool Update(Cliente t)
        {
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int pos = Finder.BinarySearchById(brHeader, t.Id, 0, n);
                if (pos < 0)
                {
                    return false;
                }

                long hpos = 8 + 4 * (pos);
                brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int id = brHeader.ReadInt32();

                long dpos = (id - 1) * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                bwData.Write(t.Id);
                bwData.Write(t.Cedula);
                bwData.Write(t.Nombres);
                bwData.Write(t.Apellidos);
                bwData.Write(t.Direccion);
                bwData.Write(t.Telefono);
                bwData.Write(t.Correo);

                Close();
                return true;
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }


        public bool Delete(Cliente t)
        {
            FileStream temporal = new FileStream("temp.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.Id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }
                Close();
                File.Delete("hCliente.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temp.dat", "hCliente.dat");

            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;

        }

        public List<Cliente> FindAll()
        {
            List<Cliente> clientes = new List<Cliente>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Cliente cliente = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    cliente= new Cliente()
                    {
                        Id = brData.ReadInt32(),
                        Cedula = brData.ReadString(),
                        Nombres = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Direccion = brData.ReadString(),
                        Telefono = brData.ReadString(),
                        Correo = brData.ReadString()
                    };

                    clientes.Add(cliente);
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return clientes;

        }


    }
}
