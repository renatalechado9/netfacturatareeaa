﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.utilities
{
    public class HeaderIndex
    {
        public int N { get; set; }
        public int K { get; set; }
        public string NameHeaderIndex { get; set; }
        public List<Index> Indices { get; set; }

    }
}
