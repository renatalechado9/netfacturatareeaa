﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.utilities
{
    public class Header
    {
        public int N { get; set; }
        public string Name { get; set; }
        public List<HeaderIndex> HeaderIndices { get; set; }

    }
}
