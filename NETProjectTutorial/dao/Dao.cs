﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface Dao<T>
    { 
        void Save(T t);
        bool Update(T t);
        bool Delete(T t);
        List<T> FindAll();
    }
}
