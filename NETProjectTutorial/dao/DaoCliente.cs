﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.dao
{
    interface DaoCliente : Dao <Cliente>
    {
        Cliente FindById(int id);
        Cliente FindByCedula(string cedula);
        List<Cliente> FindByLastname(string lastname);
    }
}
